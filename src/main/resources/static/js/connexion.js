function init(){
    if(localStorage.getItem('auth') != null)
        window.location.href="/static/html";
}

async function login(){
    const LoginRequestDTO = {
        'pseudo': document.getElementById("identifiant").value,
        'password': document.getElementById("password").value
    }

    const settings = {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
        redirect: 'follow'
    };
    console.log("ici")
    await fetch(`http://localhost:8080/public/login?pseudo=` + LoginRequestDTO.pseudo + `&password=` + LoginRequestDTO.password, settings)
        .then(response => {
            if(response.status == 200){
                console.log('res');
                localStorage.setItem('auth', btoa(LoginRequestDTO.pseudo + ":" + LoginRequestDTO.password) );
                response.text().then(result => {
                    localStorage.setItem('me', result)
                });
                window.location.href="/static/html"
            }else
                document.getElementById("error").innerHTML = "Une erreur est intervenue lors de la tentative de connexion"
    })
}

init();


// "Authorization": "Basic " + btoa(LoginRequestDTO.pseudo + ":" + LoginRequestDTO.password)