function controle() {
    const json =
    {    
        "creator": {
            "id": JSON.parse(localStorage.getItem("me")).id
        },
        "title": document.getElementById('title').value,
        "priceNew": document.getElementById('newPrice').value,
        "priceOld": document.getElementById('oldPrice').value,
        "temperature": null,
        "img_url": document.getElementById('URLImage').value,
        "link": document.getElementById('enseigne').value,
        "desc": document.getElementById('desc').value,
        "promo_code": document.getElementById('promoCode').value
    }
    console.log(localStorage.getItem("auth"));
    const settings = {
        method: 'POST',
        headers: {
            'Authorization': "Basic " + localStorage.getItem("auth"),
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(json)
    };
    fetch(`http://localhost:8080/deal`, settings)
    .then(response => {
        if(response.status == 200){
            window.location.href="/static/html"
        }
});
}