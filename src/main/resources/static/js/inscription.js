function init(){
    if(localStorage.getItem('auth') != null)
        window.location.href="/static/html";
}

async function inscription(){
    if(document.getElementById('password').value == document.getElementById('confirmation').value){
        if(document.getElementById('password').value.length < 5){
            document.getElementById('error').innerHTML = "La taille du mot de passe doit etre supérieur a 5";
            return;
        }
        document.getElementById('error').innerHTML = "";
    }else{
        document.getElementById('error').innerHTML = "La confirmation du mot de passe est incorrect";
        return;
    }
    const json = {
        "pseudo" : document.getElementById('pseudo').value,
        "lastName" : document.getElementById('last_name').value,
        "firstName" : document.getElementById('first_name').value,
        "password" : document.getElementById('password').value
    }

    const settings = {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(json)
    };

    await fetch(`http://localhost:8080/public/user`, settings)
    .then(response => {
        if(response.status == 200){
            document.getElementById('error').style.color = "rgb(0, 141, 12)";
            document.getElementById('error').innerHTML = "inscription validé";
            response.text().then(result => {
                document.getElementById('error').innerHTML += result;
            });
        }else
            document.getElementById("error").innerHTML = "Une erreur est intervenue lors de la tentative d'inscription"
    });

}

init();