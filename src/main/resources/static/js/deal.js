const div = ( object, pourcentage ) => `
        <div class="container">
            <div class="row">
                <div class="col-2">
                    <img class="img-fluid" src="${object.img_url}">
                </div>
                <div class="col-8">
                    <h1> ${object.temperature} °C</h1>
                    <h2> ${object.title} </h2>
                    <a> ${object.creator} | ${object.desc}</a>
                </div>
                <div class="col-2">
                    <a> ${object.date}</a>
                    <a href="${object.link}" class="btn btn-success">Voir</a>
                </div>
            </div>
            </br>
            <div class="row">
                <div class="col-12 border border-success rounded text-center">
                    ${object.promo_code}
                </div>
            </div>
            </br>
            <div class="row">
                <div class="col-12 border border-success rounded text-center">
                    ${object.priceOld} > ${object.priceNew} ( ${pourcentage}% de remise) 
                </div>
            </div>
            </br>
            <div class="row">
                <div class="col-12 border border-success rounded text-center">
                    ${object.desc}
                </div>
            </div>
        </div>
`
window.onload =function(){
    //document.getElementById('test').innerHTML = 
    console.log(localStorage.getItem('auth'));
    if(localStorage.getItem('auth') == null)
        document.getElementById('new_post').style.display = "none"
    else
        document.getElementById('conn_ins').style.display ="none";
}

async function getDeals() {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    let id = urlParams.get('id');
    let deals = await fetch('http://localhost:8080/public/deal/'+id);
    deals = await deals.json();
    document.getElementById('body').innerHTML += div(deals, Math.round(100-(deals.priceOld/deals.priceNew)*100));
}


getDeals();