const div = ( object ) => `
        <div class="row">
            <div class="col">
                <div class="card  style="width: 40%;"  onClick=geturl()>
                    <div class="card-body">
                        <a href="/templates/deal.html?id=${object.id}" class="stretched-link"></a>
                            <div class="row">
                                <div class="col-3">
                                    <img class="img-fluid" src="${object.img_url}">
                                </div>
                                <div class="col-6">
                                    <h2 class="temp"> ${object.temperature} °C</h2>
                                    <h2>${object.title}</h2>
                                    <a> ${object.creator.pseudo} | ${object.name}</a>
                                </div>
                                <div class="col-3">
                                    <a> ${object.date}</a>
                                    <a href="${object.link}" style="z-index: 1;" class="btn btn-success card-link " type="button">Voir</a>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    <br>
`

window.onload =function(){
    //document.getElementById('test').innerHTML = 
    console.log(localStorage.getItem('auth'));
    if(localStorage.getItem('auth') == null)
        document.getElementById('new_post').style.display = "none"
    else
        document.getElementById('conn_ins').style.display ="none";
}

 async function getDeals() {
    let deals = await fetch('http://localhost:8080/public/deal');
    deals = await deals.json()
    deals.forEach( el => {
        document.getElementById('container').innerHTML += div(el)
    })

    console.log(document.getElementsByClassName("temp"));
    var els = document.getElementsByClassName("temp")
    for(var el in els){
        if(els.item(el).innerHTML.replace("°C", "") < 50)
            els.item(el).style.color = "blue";
        else
            els.item(el).style.color = "red";
    }
}



getDeals();