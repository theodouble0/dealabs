package com.example.dealabs.metier.BO;

import com.example.dealabs.bean.DO.DealDO;
import com.example.dealabs.bean.DTO.DealDTO;
import com.example.dealabs.metier.DealMapper;
import com.example.dealabs.metier.UserMapper;
import com.example.dealabs.persistance.IDealRepository;
import com.example.dealabs.persistance.IUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.List;

@Service
public class DealBO {
    @Autowired
    private IDealRepository dealDAO;

    @Autowired
    private IUserRepository userDAO;

    @Autowired
    private TemperatureBO temperatureBO;

    public DealDTO findOne(Integer id){
        return DealMapper.map(dealDAO.getById(id), temperatureBO.findTemperatureOfPost(id));
    }

    public List<DealDTO> findAll(){
        List<DealDO> dealDOS = dealDAO.findAll(Sort.by(Sort.Direction.DESC, "date"));
        List<Integer> temp = temperatureBO.findTemperatures(dealDOS);
        return DealMapper.mapDTO(dealDOS, temp);
    }

    public DealDTO save(DealDTO deal){
        if(deal.getDate() == null)
            deal.setDate(Calendar.getInstance().getTime().toString());
        deal.setCreator(UserMapper.map(userDAO.findById(deal.getCreator().getId()).get()));
        return DealMapper.map( dealDAO.save(DealMapper.map(deal)), temperatureBO.findTemperatureOfPost(deal.getId()) ) ;
    }

    public void delete(Integer id){
        dealDAO.deleteById(id);
    }
}
