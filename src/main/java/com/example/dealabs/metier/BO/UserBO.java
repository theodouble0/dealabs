package com.example.dealabs.metier.BO;

import com.example.dealabs.bean.DO.UserDO;
import com.example.dealabs.bean.DTO.UserDTO;
import com.example.dealabs.metier.UserMapper;
import com.example.dealabs.persistance.IUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

@Service
public class UserBO {
    @Autowired
    private IUserRepository userDAO;


    public UserDO findOne(Integer id){
        return userDAO.findById(id).get();
    }


    public UserDO findOneByPseudoPassword(String pseudo, String password){
        UserDO userDO = new UserDO();
        userDO.setPseudo(pseudo);
        userDO.setPassword(password);
        return userDAO.findOne(Example.of(userDO)).orElse(null);
    }

    public UserDO save(UserDTO user){
        return userDAO.save( UserMapper.map(user) );
    }
}
