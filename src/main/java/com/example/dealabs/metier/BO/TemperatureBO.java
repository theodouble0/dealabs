package com.example.dealabs.metier.BO;

import com.example.dealabs.bean.DO.DealDO;
import com.example.dealabs.bean.DO.TemperatureDO;
import com.example.dealabs.bean.DTO.TemperatureDTO;
import com.example.dealabs.metier.DealMapper;
import com.example.dealabs.metier.TemperatureMapper;
import com.example.dealabs.metier.UserMapper;
import com.example.dealabs.persistance.IDealRepository;
import com.example.dealabs.persistance.ITemperatureRepository;
import com.example.dealabs.persistance.IUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TemperatureBO {
    @Autowired
    private ITemperatureRepository temperatureDAO;

    @Autowired
    private IDealRepository dealDAO;

    @Autowired
    private IUserRepository userDAO;

    public List<TemperatureDTO> findAll(){
        List<TemperatureDO> temperatureDOS = temperatureDAO.findAll();

        return TemperatureMapper.mapDTO(temperatureDOS);
    }

    public int findTemperatureOfPost(Integer id){
        int temp = 0;
        List<TemperatureDO> temperatureDOS = temperatureDAO.findAll();
        for(TemperatureDO temperatureDO: temperatureDOS){
            if(temperatureDO.getFk_deal().getId() == id)
                temp += temperatureDO.getValue();
        }
        return temp;
    }

    public List<Integer> findTemperatures(List<DealDO> dealDOS){
        List<Integer> count = new ArrayList<>();
        for(DealDO dealDO : dealDOS)
            count.add(findTemperatureOfPost(dealDO.getId()));
        return count;
    }

    public TemperatureDTO save(TemperatureDTO temperatureDTO){
        DealDO dealDO = dealDAO.getById(temperatureDTO.getFkDeal().getId());
        temperatureDTO.setFkDeal(DealMapper.map( dealDO, findTemperatureOfPost(dealDO.getId())));

        temperatureDTO.setFkUser(UserMapper.map(userDAO.findAll().get(temperatureDTO.getFkUser().getId())));

        return TemperatureMapper.map(temperatureDAO.save(TemperatureMapper.map(temperatureDTO)) );

    }
}
