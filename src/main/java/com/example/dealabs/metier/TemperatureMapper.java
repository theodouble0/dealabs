package com.example.dealabs.metier;

import com.example.dealabs.bean.DO.DealDO;
import com.example.dealabs.bean.DO.TemperatureDO;
import com.example.dealabs.bean.DTO.TemperatureDTO;
import com.example.dealabs.metier.BO.TemperatureBO;

import java.util.ArrayList;
import java.util.List;

public final class TemperatureMapper {
    private TemperatureMapper(){}

    public static TemperatureDTO map(TemperatureDO temperatureDO){
        TemperatureDTO temperatureDTO = new TemperatureDTO();
        temperatureDTO.setFkDeal(DealMapper.map( temperatureDO.getFk_deal(), 0) );
        temperatureDTO.setFkUser(UserMapper.map(temperatureDO.getFk_user() ) );
        temperatureDTO.setId(temperatureDO.getId());
        temperatureDTO.setValue(temperatureDO.getValue());
        return temperatureDTO;
    }

    public static List<TemperatureDTO> mapDTO(List<TemperatureDO> temperatureDOS){
        List<TemperatureDTO> temperatureDTOS = new ArrayList<>();
        int count = 0;
        for(TemperatureDO temperatureDO : temperatureDOS) {
            temperatureDTOS.add(map(temperatureDO));
            count++;
        }
        return temperatureDTOS;
    }

    public static TemperatureDO map(TemperatureDTO temperatureDTO){
        TemperatureDO temperatureDO = new TemperatureDO();
        temperatureDO.setId(temperatureDTO.getId());
        temperatureDO.setValue(temperatureDTO.getValue());
        temperatureDO.setFk_deal(DealMapper.map(temperatureDTO.getFkDeal()));
        temperatureDO.setFk_user(UserMapper.map(temperatureDTO.getFkUser()));
        return temperatureDO;
    }
}
