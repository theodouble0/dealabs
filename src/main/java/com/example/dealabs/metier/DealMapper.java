package com.example.dealabs.metier;

import com.example.dealabs.bean.DO.DealDO;
import com.example.dealabs.bean.DO.UserDO;
import com.example.dealabs.bean.DTO.DealDTO;
import com.example.dealabs.metier.BO.TemperatureBO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.*;

public final class DealMapper {
    private DealMapper(){}

    public static DealDTO map(DealDO dealDO, Integer temp){
        DealDTO dealDTO = new DealDTO();
        dealDTO.setId(dealDO.getId());
        dealDTO.setCreator(UserMapper.map(dealDO.getCreator()));
        SimpleDateFormat format1 = new SimpleDateFormat("dd-MM-yyyy");
        dealDTO.setDate(format1.format(dealDO.getDate().getTime()));
        dealDTO.setDesc(dealDO.getDesc());
        dealDTO.setLink(dealDO.getLink());
        dealDTO.setImg_url(dealDO.getImg_url());
        dealDTO.setPriceNew(dealDO.getPriceNew());
        dealDTO.setPriceOld(dealDO.getPriceOld());
        dealDTO.setName(dealDO.getName());
        dealDTO.setPromo_code(dealDO.getPromo_code());
        dealDTO.setTemperature( temp );
        dealDTO.setTitle(dealDO.getTitle());
        return dealDTO;
    }


    public static DealDO map(DealDTO dealDTO){
        DealDO dealDO = new DealDO();
        dealDO.setId(dealDTO.getId());
        dealDO.setCreator(UserMapper.map(dealDTO.getCreator()));
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.FRENCH);
        try {
            cal.setTime(sdf.parse(dealDTO.getDate()));
        }catch (Exception e){}
        dealDO.setDate(cal);
        dealDO.setDesc(dealDTO.getDesc());
        dealDO.setLink(dealDTO.getLink());
        dealDO.setImg_url(dealDTO.getImg_url());
        dealDO.setPriceNew(dealDTO.getPriceNew());
        dealDO.setPriceOld(dealDTO.getPriceOld());
        dealDO.setName(dealDTO.getName());
        dealDO.setPromo_code(dealDTO.getPromo_code());
        dealDO.setTemperature(dealDTO.getTemperature());
        dealDO.setTitle(dealDTO.getTitle());
        return dealDO;
    }

    public static List<DealDTO> mapDTO(List<DealDO> dealDOS, List<Integer> temps){
        List<DealDTO> dealDTOS = new ArrayList<DealDTO>();
        int tmp = 0;
        for(DealDO deals : dealDOS){
            dealDTOS.add( DealMapper.map(deals, temps.get(tmp)) );
            tmp++;
        }
        return dealDTOS;
    }

    public static List<DealDO> mapDO(List<DealDTO> dealDTOs){
        List<DealDO> dealDOS = new ArrayList<DealDO>();
        for(DealDTO deals : dealDTOs){
            dealDOS.add( DealMapper.map(deals) );
        }
        return dealDOS;
    }
}
