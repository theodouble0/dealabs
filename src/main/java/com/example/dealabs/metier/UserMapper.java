package com.example.dealabs.metier;

import com.example.dealabs.bean.DO.UserDO;
import com.example.dealabs.bean.DTO.UserDTO;

public final class UserMapper {
    private UserMapper(){}

    public static UserDTO map(UserDO userDO){
        UserDTO userDTO = new UserDTO();
        userDTO.setId(userDO.getId());
        userDTO.setFirstName(userDO.getFirstName());
        userDTO.setLastName(userDO.getLastName());
        userDTO.setPseudo(userDO.getPseudo());
        userDTO.setPassword(userDO.getPassword());
        return userDTO;
    }

    public static UserDO map(UserDTO userDTO){
        UserDO userDO = new UserDO();
        userDO.setId(userDTO.getId());
        userDO.setFirstName(userDTO.getFirstName());
        userDO.setLastName(userDTO.getLastName());
        userDO.setPassword(userDTO.getPassword());
        userDO.setPseudo(userDTO.getPseudo());
        return userDO;
    }
}
