package com.example.dealabs.metier.Controller;

import com.example.dealabs.bean.DTO.DealDTO;
import com.example.dealabs.metier.BO.DealBO;
import com.example.dealabs.metier.BO.UserBO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
public class DealController {
    @Autowired
    private DealBO dealBO;

    @Autowired
    private UserBO userBO;

    @RequestMapping(value = "/public/deal", method = RequestMethod.GET)
    public List<DealDTO> findAll() {
        List<DealDTO> dealDTO = dealBO.findAll();
        return dealDTO;
    }

    @RequestMapping(value = "/public/deal/{id}", method = RequestMethod.GET)
    public DealDTO findOne(@PathVariable Integer id) {
        DealDTO dealDTO = dealBO.findOne(id);
        return dealDTO;
    }

    @RequestMapping(value ="/deal",  method = RequestMethod.POST)
    public DealDTO save(@RequestBody DealDTO dealDTO) {
        DealDTO dealDTOSave = dealBO.save(dealDTO);
        return dealDTOSave;
    }

    @RequestMapping(value = "/deal/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable Integer id) {
        dealBO.delete(id);
    }
}