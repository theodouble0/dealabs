package com.example.dealabs.metier.Controller;

import com.example.dealabs.bean.DO.TemperatureDO;
import com.example.dealabs.bean.DTO.TemperatureDTO;
import com.example.dealabs.metier.BO.TemperatureBO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(value = "/temperature")
public class TemperatureController {
    @Autowired
    private TemperatureBO temperatureBO;

    @RequestMapping(method = RequestMethod.GET)
    public List<TemperatureDTO> findAll(){
        return temperatureBO.findAll();
    }

    @RequestMapping(method = RequestMethod.POST)
    public TemperatureDTO save(@RequestBody TemperatureDTO temperatureDTO){
        TemperatureDTO temperatureDTO1 = temperatureBO.save(temperatureDTO);
        return temperatureDTO1;
    }
}
