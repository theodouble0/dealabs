package com.example.dealabs.metier.Controller;

import com.example.dealabs.bean.DO.UserDO;
import com.example.dealabs.bean.DTO.UserDTO;
import com.example.dealabs.metier.BO.UserBO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
public class UserController {
    @Autowired
    private UserBO userBO;

    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    public UserDO findOne(@PathVariable Integer id) {
        UserDO userDO = userBO.findOne(id);
        return userDO;
    }

    @RequestMapping(value = "/public/user", method = RequestMethod.POST)
    public UserDO save(@RequestBody UserDTO user) {
        UserDO userDO = userBO.save(user);
        return userDO;
    }
}
