package com.example.dealabs.security;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import com.example.dealabs.bean.DTO.UserDTO;
import com.example.dealabs.metier.BO.UserBO;
import com.example.dealabs.metier.UserMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author MDE
 *
 */
@RestController
@RequestMapping(value = "/public/login")
@Transactional
public class LoginBD {

	private static final Logger logger = LoggerFactory.getLogger(LoginBD.class);

	@Autowired
	private AuthenticationProvider authenticationManager;

	@Autowired
	private UserBO userBO;


	/**
	 * methode de connexion d'un utilisateur
	 *
	 * @param request données necessaire a la connexion
	 * @return
	 * @throws RestException
	 * @throws Exception
	 */
	@RequestMapping(method = RequestMethod.GET)
	public UserDTO login( final UserDTO request, final HttpServletRequest req) {

		// Controle des params obligatoires
		if (StringUtils.isEmpty(request.getPseudo()) || StringUtils.isEmpty(request.getPassword())) {
			throw new BadCredentialsException("User/pwd must not be emtpy");

		}
		final Authentication authentication = authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken(request.getPseudo(), request.getPassword()));
		if (authentication == null) {
			throw new BadCredentialsException("User/pwd incorrect");
		}

		final DlabsSpringUser utilisateur = (DlabsSpringUser) authentication.getPrincipal();
		logger.debug("New user logged : " + utilisateur.getUsername());

		UserDTO user = UserMapper.map(userBO.findOneByPseudoPassword(request.getPseudo(), request.getPassword()));
		// TODO : Ajouter les informations n�cessaires au DTO pour retour au front.
		//return new UserDTO();
		return user;
	}

}
