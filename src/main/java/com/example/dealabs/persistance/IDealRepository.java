package com.example.dealabs.persistance;

import com.example.dealabs.bean.DO.DealDO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface IDealRepository extends JpaRepository<DealDO, Integer> {
}
