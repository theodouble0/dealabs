package com.example.dealabs.persistance;

import com.example.dealabs.bean.DO.UserDO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface IUserRepository extends JpaRepository<UserDO, Integer> {
}
