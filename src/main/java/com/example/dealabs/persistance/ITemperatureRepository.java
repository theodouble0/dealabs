package com.example.dealabs.persistance;

import com.example.dealabs.bean.DO.TemperatureDO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface ITemperatureRepository extends JpaRepository<TemperatureDO, Integer> {
}
