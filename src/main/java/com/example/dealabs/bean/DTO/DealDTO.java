package com.example.dealabs.bean.DTO;

import com.example.dealabs.bean.DO.UserDO;

import javax.persistence.Column;
import java.util.Calendar;
import java.util.List;

public class DealDTO{
    private Integer id;
    private String title;
    private String name;
    private String link;
    private Float priceOld;
    private Float priceNew;
    private String promo_code;
    private Float temperature;
    private UserDTO creator;
    private String date;
    private String img_url;
    private String desc;

    public DealDTO() {
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setPriceOld(float priceOld) {
        this.priceOld = priceOld;
    }

    public void setPriceNew(float priceNew) {
        this.priceNew = priceNew;
    }

    public void setPromo_code(String promo_code) {
        this.promo_code = promo_code;
    }

    public void setTemperature(float temperature) {
        this.temperature = temperature;
    }

    public void setCreator(UserDTO creator) {
        this.creator = creator;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getName() {
        return name;
    }

    public String getLink() {
        return link;
    }

    public float getPriceOld() {
        return priceOld;
    }

    public float getPriceNew() {
        return priceNew;
    }

    public String getPromo_code() {
        return promo_code;
    }

    public float getTemperature() {
        return temperature;
    }

    public UserDTO getCreator() {
        return creator;
    }

    public String getDate() {
        return date;
    }

    public String getImg_url() {
        return img_url;
    }

    public String getDesc() {
        return desc;
    }
}
