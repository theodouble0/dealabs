package com.example.dealabs.bean.DTO;

import com.example.dealabs.bean.DO.DealDO;
import com.example.dealabs.bean.DO.UserDO;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

public class TemperatureDTO {
    private Integer id;
    private int value;
    private DealDTO fkDeal;
    private UserDTO fkUser;

    public Integer getId() {
        return id;
    }

    public int getValue() {
        return value;
    }

    public UserDTO getFkUser() {
        return fkUser;
    }

    public DealDTO getFkDeal() {
        return fkDeal;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void setFkUser(UserDTO fkUser) {
        this.fkUser = fkUser;
    }

    public void setFkDeal(DealDTO fkDeal) {
        this.fkDeal = fkDeal;
    }

}
