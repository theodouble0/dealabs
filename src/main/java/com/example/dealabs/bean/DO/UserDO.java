package com.example.dealabs.bean.DO;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import java.util.List;

@Repository
@Entity
@Table(name="user")
public class UserDO {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="id")
    private Integer id;

    @Column(name="pseudo")
    private String pseudo;

    @Column(name="first_name")
    private String firstName;

    @Column(name="last_name")
    private String lastName;

    @Column(name="password")
    private String password;

    @OneToMany(targetEntity=DealDO.class, mappedBy="creator",cascade=CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnore
    private List<DealDO> creator;

    @OneToMany(targetEntity=TemperatureDO.class,mappedBy = "fkUser", cascade=CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnore
    private List<TemperatureDO> vote;

    public List<DealDO> getCreator() {
        return creator;
    }
    public void setCreator(List<DealDO> creator) {
        this.creator = creator;
    }

    public Integer getId() {
        return id;
    }

    public String getPseudo() {
        return pseudo;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
