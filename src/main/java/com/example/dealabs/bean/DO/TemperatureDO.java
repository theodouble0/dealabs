package com.example.dealabs.bean.DO;

import org.springframework.stereotype.Repository;

import javax.persistence.*;
import java.util.List;

@Repository
@Entity
@Table(name="temperature")
public class TemperatureDO {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="id")
    private Integer id;

    @Column(name="value", nullable=false)
    private int value;

    /*@Column(name="FK_user", nullable=false)
    private String fkUser;

    @Column(name="FK_deal", nullable=false)
    private String fkDeal;*/

    public Integer getId() {
        return id;
    }

    public int getValue() {
        return value;
    }

    /*public String getFkUser() {
        return fkUser;
    }

    public String getFkDeal() {
        return fkDeal;
    }*/

    public void setId(Integer id) {
        this.id = id;
    }

    public void setValue(int value) {
        this.value = value;
    }

    /*public void setFkUser(String fkUser) {
        this.fkUser = fkUser;
    }

    public void setFkDeal(String fkDeal) {
        this.fkDeal = fkDeal;
    }*/

    @ManyToOne( cascade={CascadeType.MERGE})
    @JoinColumn(name = "fkDeal", referencedColumnName = "id")
    private DealDO fkDeal;

    @ManyToOne( cascade={CascadeType.MERGE})
    @JoinColumn(name = "fkUser", referencedColumnName = "id")
    private UserDO fkUser;

    public DealDO getFk_deal() {
        return fkDeal;
    }

    public UserDO getFk_user() {
        return fkUser;
    }

    public void setFk_deal(DealDO fk_deal) {
        this.fkDeal = fk_deal;
    }

    public void setFk_user(UserDO fk_user) {
        this.fkUser = fk_user;
    }
}
