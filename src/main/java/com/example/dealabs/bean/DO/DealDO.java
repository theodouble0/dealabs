package com.example.dealabs.bean.DO;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import java.util.Calendar;
import java.util.List;

@Repository
@Entity
@Table(name="Deal")
public class DealDO {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
    private Integer id;

    @Column(name="title", nullable=false)
    private String title;

    @Column(name="shop_name")
    private String name;

    @Column(name="shop_link")
    private String link;

    @Column(name="price_old")
    private Float priceOld;

    @Column(name="price_new")
    private Float priceNew;

    @Column(name="promo_code")
    private String promo_code;

    @Column(name="temperature")
    private Float temperature;
/*
    @Column(name="creator")
    private String creator;
*/
    @Column(name="date")
    private Calendar date;

    @Column(name="img_url")
    private String img_url;

    @Column(name="description")
    private String desc;

    public DealDO() {
    }

// Getters et setters

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getName() {
        return name;
    }

    public String getLink() {
        return link;
    }

    public Float getPriceOld() {
        return priceOld;
    }

    public Float getPriceNew() {
        return priceNew;
    }

    public String getPromo_code() {
        return promo_code;
    }

    public Float getTemperature() {
        return temperature;
    }

    public Calendar getDate() {
        return date;
    }

    public String getImg_url() {
        return img_url;
    }

    public String getDesc() {
        return desc;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setPriceOld(float priceOld) {
        this.priceOld = priceOld;
    }

    public void setPriceNew(float priceNew) {
        this.priceNew = priceNew;
    }

    public void setPromo_code(String promo_code) {
        this.promo_code = promo_code;
    }

    public void setTemperature(float temperature) {
        this.temperature = temperature;
    }



    public void setDate(Calendar date) {
        this.date = date;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @ManyToOne()
    @JoinColumn(name="creator", referencedColumnName = "id")
    private UserDO creator;

    public void setCreator(UserDO creator) {
        this.creator = creator;
    }

    public UserDO getCreator() {
        return creator;
    }

    @OneToMany(targetEntity=TemperatureDO.class, mappedBy="fkDeal",cascade=CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnore
    private List<TemperatureDO> vote;
}
